#pragma once

#pragma pack(push)
struct VertexPC
{
    vec3 position;
    vec3 color;
};

struct VertexPNT
{
    vec3 position;
    vec3 normal;
    vec2 textureCoord;
};

struct Material
{
    vec3 baseColor;
    vec3 emission;
    f32 roughness;
};
#pragma pack(pop)

struct MeshData
{
    VertexPNT *vertices;
    u32 *indices;
    u32 vertexCount;
    u32 indexCount;
};
