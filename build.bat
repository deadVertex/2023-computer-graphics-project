@echo off

set CONFIG=Debug
cmake --build build --config %CONFIG% --target main shaders
cmake --install build --prefix . --config %CONFIG%

REM cmake --build cmake_build --config Debug --target test_runner
REM ctest --test-dir cmake_build\unit_tests -C Debug --output-on-failure
