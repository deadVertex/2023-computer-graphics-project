#include "vulkan_utils.cpp"

#define VALIDATION_LAYER_NAME "VK_LAYER_KHRONOS_validation"

inline b32 HasValidationLayerSupport()
{
    b32 found = false;

    VkLayerProperties layers[256];
    u32 layerCount = ArrayCount(layers);
    VK_CHECK(vkEnumerateInstanceLayerProperties(&layerCount, layers));

    for (u32 layerIndex = 0; layerIndex < layerCount; ++layerIndex)
    {
        if (strcmp(VALIDATION_LAYER_NAME, layers[layerIndex].layerName) == 0)
        {
            found = true;
            break;
        }
    }

    return found;
}

internal VkInstance VulkanCreateInstance()
{
    const char *validationLayers[] = {VALIDATION_LAYER_NAME};
    const char *requestedExtensions[] = {VK_EXT_DEBUG_UTILS_EXTENSION_NAME};

    u32 glfwExtensionsCount = 0;
    const char **glfwExtensions =
        glfwGetRequiredInstanceExtensions(&glfwExtensionsCount);

    const char *extensions[16];
    u32 extensionCount = 0;
    for (u32 i = 0; i < ArrayCount(requestedExtensions); ++i)
    {
        Assert(extensionCount < ArrayCount(extensions));
        extensions[extensionCount++] = requestedExtensions[i];
    }

    for (u32 i = 0; i < glfwExtensionsCount; ++i)
    {
        Assert(extensionCount < ArrayCount(extensions));
        extensions[extensionCount++] = glfwExtensions[i];
    }

    VkApplicationInfo appInfo = {VK_STRUCTURE_TYPE_APPLICATION_INFO};
    appInfo.apiVersion = VK_API_VERSION_1_2;

    VkInstanceCreateInfo instanceCreateInfo = {
        VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO};
    instanceCreateInfo.pApplicationInfo = &appInfo;
    instanceCreateInfo.enabledExtensionCount = extensionCount;
    instanceCreateInfo.ppEnabledExtensionNames = extensions;
#ifdef ENABLE_VALIDATION_LAYERS
    LogMessage("Enabling Vulkan validation layers");
    if (HasValidationLayerSupport())
    {
        instanceCreateInfo.enabledLayerCount = ArrayCount(validationLayers);
        instanceCreateInfo.ppEnabledLayerNames = validationLayers;
    }
    else
    {
        LogMessage("Vulkan instance does not support validation layers");
    }
#endif

    VkInstance instance;
    VK_CHECK(vkCreateInstance(&instanceCreateInfo, NULL, &instance));

    return instance;
}

internal VkDevice VulkanCreateDevice(VkInstance instance,
    VkPhysicalDevice physicalDevice,
    VulkanQueueFamilyIndices queueFamilyIndices)
{
    float queuePriorities[] = {1.0f};

    u32 queueCreateInfoCount = 1;
    VkDeviceQueueCreateInfo queueCreateInfos[2] = {};
    queueCreateInfos[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfos[0].queueFamilyIndex = queueFamilyIndices.graphicsQueue;
    queueCreateInfos[0].queueCount = 1;
    queueCreateInfos[0].pQueuePriorities = queuePriorities;

    if (queueFamilyIndices.presentQueue != queueFamilyIndices.graphicsQueue)
    {
        queueCreateInfos[1].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfos[1].queueFamilyIndex = queueFamilyIndices.presentQueue;
        queueCreateInfos[1].queueCount = 1;
        queueCreateInfos[1].pQueuePriorities = queuePriorities;
        queueCreateInfoCount = 2;
    }

    const char *extensions[] = {VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        VK_EXT_SHADER_VIEWPORT_INDEX_LAYER_EXTENSION_NAME};

    VkPhysicalDeviceVulkan12Features enabledFeaturesVulkan12 = {
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES};
    enabledFeaturesVulkan12.shaderOutputViewportIndex = VK_TRUE;
    enabledFeaturesVulkan12.shaderOutputLayer = VK_TRUE;

    // TODO: Not sure if we need to test if the feature is supported?
    VkPhysicalDeviceFeatures2 enabledFeatures = {
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2};
    enabledFeatures.pNext = &enabledFeaturesVulkan12;
    enabledFeatures.features.fillModeNonSolid = VK_TRUE;
    enabledFeatures.features.samplerAnisotropy = VK_TRUE;
    enabledFeatures.features.shaderSampledImageArrayDynamicIndexing = VK_TRUE;

    VkDeviceCreateInfo deviceCreateInfo = {
        VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO};
    deviceCreateInfo.pNext = &enabledFeatures;
    deviceCreateInfo.ppEnabledExtensionNames = extensions;
    deviceCreateInfo.enabledExtensionCount = ArrayCount(extensions);
    deviceCreateInfo.queueCreateInfoCount = queueCreateInfoCount;
    deviceCreateInfo.pQueueCreateInfos = queueCreateInfos;
    // deviceCreateInfo.pEnabledFeatures = &enabledFeatures;

    VkDevice device;
    VK_CHECK(vkCreateDevice(physicalDevice, &deviceCreateInfo, NULL, &device));

    return device;
}

internal VkSwapchainKHR VulkanCreateSwapchain(VkDevice device,
    VkSurfaceKHR surface, u32 imageCount, u32 framebufferWidth,
    u32 framebufferHeight, VulkanQueueFamilyIndices queueFamilyIndices)
{
    u32 queueFamilyIndicesArray[2] = {
        queueFamilyIndices.graphicsQueue, queueFamilyIndices.presentQueue};

    VkSwapchainCreateInfoKHR createInfo = {
        VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR};
    createInfo.surface = surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = VK_FORMAT_B8G8R8A8_UNORM;
    createInfo.imageColorSpace =
        VK_COLOR_SPACE_SRGB_NONLINEAR_KHR; // FIXME: Supported?
    createInfo.imageExtent.width = framebufferWidth;
    createInfo.imageExtent.height = framebufferHeight;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    if (queueFamilyIndices.graphicsQueue != queueFamilyIndices.presentQueue)
    {
        // VK_SHARING_MODE_EXCLUSIVE is defined as 0, so we don't need to
        // handle it
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = ArrayCount(queueFamilyIndicesArray);
        createInfo.pQueueFamilyIndices = queueFamilyIndicesArray;
    }
    createInfo.presentMode =
        VK_PRESENT_MODE_FIFO_KHR; // VK_PRESENT_MODE_IMMEDIATE_KHR; // FIXME:
                                  // Always supported?
    createInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    VkSwapchainKHR swapchain;
    VK_CHECK(vkCreateSwapchainKHR(device, &createInfo, NULL, &swapchain));
    return swapchain;
}
internal VulkanSwapchain VulkanSetupSwapchain(VkDevice device,
    VkPhysicalDevice physicalDevice, VkSurfaceKHR surface,
    VulkanQueueFamilyIndices queueFamilyIndices, VkRenderPass renderPass,
    u32 imageCount, u32 width, u32 height)
{
    VulkanSwapchain swapchain = {};
    swapchain.width = width;
    swapchain.height = height;
    swapchain.imageCount = imageCount;

    Assert(imageCount <= ArrayCount(swapchain.images));

    swapchain.handle = VulkanCreateSwapchain(
        device, surface, imageCount, width, height, queueFamilyIndices);

    VK_CHECK(vkGetSwapchainImagesKHR(
        device, swapchain.handle, &swapchain.imageCount, NULL));

    // TODO: Probably don't need to assert this, rather just check that we can
    // store the returned imageCount
    Assert(swapchain.imageCount == imageCount);
    VK_CHECK(vkGetSwapchainImagesKHR(
        device, swapchain.handle, &swapchain.imageCount, swapchain.images));

    swapchain.depthImage =
        VulkanCreateImage(device, physicalDevice, width, height,
            VK_FORMAT_D32_SFLOAT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);

    for (u32 i = 0; i < swapchain.imageCount; ++i)
    {
        // TODO: Get supported image formats for the surface?
        swapchain.imageViews[i] =
            VulkanCreateImageView(device, swapchain.images[i],
                VK_FORMAT_B8G8R8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
        swapchain.framebuffers[i] = VulkanCreateFramebuffer(device, renderPass,
            swapchain.imageViews[i], swapchain.depthImage.view, width, height);
    }

    return swapchain;
}

struct DescriptorSetLayoutBindingStack
{
    VkDescriptorSetLayoutBinding bindings[16];
    u32 count;
};

inline void PushDescriptorSetLayoutBinding(DescriptorSetLayoutBindingStack *stack,
        u32 bindingIndex, VkDescriptorType descriptorType, u32 descriptorCount,
        u32 stageFlags)
{
    Assert(stack->count < ArrayCount(stack->bindings));

    VkDescriptorSetLayoutBinding *binding = stack->bindings + stack->count++;
    binding->binding = bindingIndex;
    binding->descriptorType = descriptorType;
    binding->descriptorCount = descriptorCount;
    binding->stageFlags = stageFlags;
}

inline void PushDescriptorSetLayoutBinding(DescriptorSetLayoutBindingStack *stack,
        u32 bindingIndex, VkDescriptorType descriptorType, u32 descriptorCount,
        u32 stageFlags, VkSampler sampler)
{
    Assert(descriptorType == VK_DESCRIPTOR_TYPE_SAMPLER);

    Assert(stack->count < ArrayCount(stack->bindings));

    VkDescriptorSetLayoutBinding *binding = stack->bindings + stack->count++;
    binding->binding = bindingIndex;
    binding->descriptorType = descriptorType;
    binding->descriptorCount = descriptorCount;
    binding->stageFlags = stageFlags;
    binding->pImmutableSamplers = &sampler;
}

internal VkDescriptorSetLayout VulkanCreateDescriptorSetLayout(
    VkDevice device, VkSampler defaultSampler)
{
    DescriptorSetLayoutBindingStack stack = {};
    PushDescriptorSetLayoutBinding(&stack, 0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        1, VK_SHADER_STAGE_VERTEX_BIT);
    PushDescriptorSetLayoutBinding(&stack, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        1, VK_SHADER_STAGE_VERTEX_BIT);
    PushDescriptorSetLayoutBinding(&stack, 2, VK_DESCRIPTOR_TYPE_SAMPLER,
        1, VK_SHADER_STAGE_FRAGMENT_BIT, defaultSampler);
    PushDescriptorSetLayoutBinding(&stack, 3, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
        1, VK_SHADER_STAGE_FRAGMENT_BIT);
    PushDescriptorSetLayoutBinding(&stack, 4, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        1, VK_SHADER_STAGE_VERTEX_BIT);

    VkDescriptorSetLayoutCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO};
    createInfo.bindingCount = stack.count;
    createInfo.pBindings = stack.bindings;

    VkDescriptorSetLayout descriptorSetLayout;

    VK_CHECK(vkCreateDescriptorSetLayout(
        device, &createInfo, NULL, &descriptorSetLayout));

    return descriptorSetLayout;
}

internal VkPipelineLayout VulkanCreatePipelineLayout(VkDevice device,
    VkDescriptorSetLayout descriptorSetLayout, b32 usePushConstants)
{
    VkPushConstantRange pushConstantRange = {};
    pushConstantRange.offset = 0;
    pushConstantRange.size = sizeof(MeshPushConstants);
    pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    VkPipelineLayoutCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO};
    createInfo.setLayoutCount = 1;
    createInfo.pSetLayouts = &descriptorSetLayout;
    if (usePushConstants)
    {
        createInfo.pPushConstantRanges = &pushConstantRange;
        createInfo.pushConstantRangeCount = 1;
    }

    VkPipelineLayout layout;
    VK_CHECK(vkCreatePipelineLayout(device, &createInfo, 0, &layout));
    return layout;
}

struct DescriptorSetWritesStack
{
    VkWriteDescriptorSet writes[16];
    u32 count;
};

inline void PushDescriptorSetWrite(DescriptorSetWritesStack *stack,
        VkDescriptorSet dstSet, u32 dstBinding,
        VkDescriptorType descriptorType,
        u32 descriptorCount,
        VkDescriptorBufferInfo *bufferInfo)
{
    Assert(stack->count < ArrayCount(stack->writes));
    VkWriteDescriptorSet *write = stack->writes + stack->count++;

    write->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write->dstSet = dstSet;
    write->dstBinding = dstBinding;
    write->descriptorType = descriptorType;
    write->descriptorCount = descriptorCount;
    write->pBufferInfo = bufferInfo;
}

inline void PushDescriptorSetWrite(DescriptorSetWritesStack *stack,
        VkDescriptorSet dstSet, u32 dstBinding,
        VkDescriptorType descriptorType,
        u32 descriptorCount,
        VkDescriptorImageInfo *imageInfo)
{
    Assert(stack->count < ArrayCount(stack->writes));
    VkWriteDescriptorSet *write = stack->writes + stack->count++;

    write->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write->dstSet = dstSet;
    write->dstBinding = dstBinding;
    write->descriptorType = descriptorType;
    write->descriptorCount = descriptorCount;
    write->pImageInfo = imageInfo;
}

internal void UpdateMeshDescriptorSets(
    VulkanRenderer *renderer, VulkanPipeline *pipeline)
{
    Assert(ArrayCount(pipeline->descriptorSets) == 2);

    for (u32 i = 0; i < 2; ++i)
    {
        DescriptorSetWritesStack stack = {};

        VkDescriptorBufferInfo vertexDataBufferInfo = {};
        vertexDataBufferInfo.buffer = renderer->vertexDataBuffer.handle;
        vertexDataBufferInfo.range = VK_WHOLE_SIZE;

        VkDescriptorBufferInfo cameraDataBufferInfo = {};
        cameraDataBufferInfo.buffer = renderer->cameraDataBuffer.handle;
        cameraDataBufferInfo.range = VK_WHOLE_SIZE;

        VkDescriptorImageInfo imageInfo = {};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = renderer->testImage.view;

        VkDescriptorBufferInfo modelMatrixBufferInfo = {};
        modelMatrixBufferInfo.buffer = renderer->modelMatrixBuffer.handle;
        modelMatrixBufferInfo.range = VK_WHOLE_SIZE;

        PushDescriptorSetWrite(&stack, pipeline->descriptorSets[i], 0,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, &vertexDataBufferInfo);
        PushDescriptorSetWrite(&stack, pipeline->descriptorSets[i], 1,
            VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, &cameraDataBufferInfo);
        PushDescriptorSetWrite(&stack, pipeline->descriptorSets[i], 3,
            VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1, &imageInfo);
        PushDescriptorSetWrite(&stack, pipeline->descriptorSets[i], 4,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, &modelMatrixBufferInfo);

        vkUpdateDescriptorSets(
            renderer->device, stack.count, stack.writes, 0, NULL);
    }
}

internal VulkanPipeline CreatePipeline(VulkanRenderer *renderer,
    VkShaderModule vertexShader, VkShaderModule fragmentShader)
{
    VulkanPipeline pipeline = {};

    pipeline.descriptorSetLayout = VulkanCreateDescriptorSetLayout(
        renderer->device, renderer->defaultSampler);
    pipeline.layout = VulkanCreatePipelineLayout(
        renderer->device, pipeline.descriptorSetLayout, true);

    VulkanPipelineDefinition pipelineDefinition = {};
    pipelineDefinition.vertexStride = sizeof(VertexPNT);
    pipelineDefinition.primitive = Primitive_Triangle;
    pipelineDefinition.polygonMode = PolygonMode_Fill;
    pipelineDefinition.cullMode = CullMode_None;
    pipelineDefinition.depthTestEnabled = true;
    pipelineDefinition.depthWriteEnabled = true;

    pipeline.handle = VulkanCreatePipeline(renderer->device,
        renderer->pipelineCache, renderer->renderPass, pipeline.layout,
        vertexShader, fragmentShader, &pipelineDefinition);

    VkDescriptorSetLayout layouts[2] = {
        pipeline.descriptorSetLayout, pipeline.descriptorSetLayout};
    Assert(ArrayCount(layouts) == ArrayCount(pipeline.descriptorSets));
    VulkanAllocateDescriptorSets(renderer->device, renderer->descriptorPool,
        layouts, ArrayCount(layouts), pipeline.descriptorSets);

    UpdateMeshDescriptorSets(renderer, &pipeline);

    return pipeline;
}

internal VkDescriptorPool VulkanCreateDescriptorPool(VkDevice device)
{
    VkDescriptorPoolSize poolSizes[6];
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSizes[0].descriptorCount = 64;
    poolSizes[1].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    poolSizes[1].descriptorCount = 64;
    poolSizes[2].type = VK_DESCRIPTOR_TYPE_SAMPLER;
    poolSizes[2].descriptorCount = 64;
    poolSizes[3].type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    poolSizes[3].descriptorCount = 256;
    poolSizes[4].type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    poolSizes[4].descriptorCount = 64;
    poolSizes[5].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[5].descriptorCount = 64;

    VkDescriptorPoolCreateInfo createInfo = {VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO};
    createInfo.poolSizeCount = ArrayCount(poolSizes);
    createInfo.pPoolSizes = poolSizes;
    createInfo.maxSets = 64;
    // TODO: Spec doesn't make this sound particularly efficient. It seems like
    // we should rather have a separate pool for descriptor sets which we want
    // to free and then just do a bulk reset for them.
    createInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;

    VkDescriptorPool descriptorPool;
    VK_CHECK(vkCreateDescriptorPool(device, &createInfo, NULL, &descriptorPool));
    return descriptorPool;
}

internal VulkanImage UploadHdrImageToGPU(
    VulkanRenderer *renderer, HdrImage image)
{
    // Create image
    VkFormat format = VK_FORMAT_R32G32B32A32_SFLOAT;
    VulkanImage texture = VulkanCreateImage(renderer->device,
        renderer->physicalDevice, image.width, image.height, format,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_COLOR_BIT);

    // Map memory
    MemoryArena textureUploadArena = {};
    InitializeMemoryArena(&textureUploadArena,
        renderer->textureUploadBuffer.data, TEXTURE_UPLOAD_BUFFER_SIZE);

    // Copy data to upload buffer
    f32 *pixels =
        AllocateArray(&textureUploadArena, f32, image.width * image.height * 4);
    CopyMemory(
        pixels, image.pixels, image.width * image.height * sizeof(f32) * 4);

    // Update image
    VulkanTransitionImageLayout(texture.handle,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, renderer->device,
        renderer->commandPool, renderer->graphicsQueue);

    VulkanCopyBufferToImage(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->textureUploadBuffer.handle,
        texture.handle, image.width, image.height, 0);

    VulkanTransitionImageLayout(texture.handle,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, renderer->device,
        renderer->commandPool, renderer->graphicsQueue);

    return texture;
}

internal VulkanRenderer CreateVulkanRenderer(GLFWwindow *window)
{
    VulkanRenderer renderer = {};

    renderer.instance = VulkanCreateInstance();
    renderer.messenger = VulkanRegisterDebugCallback(renderer.instance);
    renderer.physicalDevice = VulkanSelectPhysicalDevice(renderer.instance);

    VK_CHECK(glfwCreateWindowSurface(
        renderer.instance, window, NULL, &renderer.surface));

    renderer.queueFamilyIndices =
        VulkanGetQueueFamilies(renderer.physicalDevice, renderer.surface);
    renderer.device = VulkanCreateDevice(renderer.instance,
        renderer.physicalDevice, renderer.queueFamilyIndices);

    // Retrieve graphics and present queue
    vkGetDeviceQueue(renderer.device, renderer.queueFamilyIndices.graphicsQueue,
        0, &renderer.graphicsQueue);
    vkGetDeviceQueue(renderer.device, renderer.queueFamilyIndices.presentQueue,
        0, &renderer.presentQueue);

    // FIXME: Query surface formats and present modes
    VkSurfaceCapabilitiesKHR surfaceCapabilities = {
        VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR};
    VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
        renderer.physicalDevice, renderer.surface, &surfaceCapabilities));
    Assert(surfaceCapabilities.currentTransform &
           VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR);

    VkSurfaceFormatKHR surfaceFormats[8];
    u32 surfaceFormatCount = ArrayCount(surfaceFormats);
    VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(renderer.physicalDevice,
        renderer.surface, &surfaceFormatCount, surfaceFormats));
    Assert(surfaceFormats[0].format == VK_FORMAT_B8G8R8A8_UNORM);
    Assert(surfaceFormats[0].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR);

    // Create render pass
    VulkanRenderPassSpec displayRenderPassSpec = {};
    displayRenderPassSpec.colorAttachmentFormat = surfaceFormats[0].format;
    displayRenderPassSpec.colorAttachmentFinalLayout =
        VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    displayRenderPassSpec.useDepth = true;
    renderer.renderPass =
        VulkanCreateRenderPass(renderer.device, displayRenderPassSpec);

    renderer.swapchain = VulkanSetupSwapchain(renderer.device,
        renderer.physicalDevice, renderer.surface, renderer.queueFamilyIndices,
        renderer.renderPass, 2, surfaceCapabilities.currentExtent.width,
        surfaceCapabilities.currentExtent.height);

    renderer.acquireSemaphore = VulkanCreateSemaphore(renderer.device);
    renderer.releaseSemaphore = VulkanCreateSemaphore(renderer.device);

    renderer.commandPool = VulkanCreateCommandPool(
        renderer.device, renderer.queueFamilyIndices.graphicsQueue);

    renderer.descriptorPool = VulkanCreateDescriptorPool(renderer.device);

    renderer.commandBuffer =
        VulkanAllocateCommandBuffer(renderer.device, renderer.commandPool);

    renderer.vertexDataUploadBuffer =
        VulkanCreateBuffer(renderer.device, renderer.physicalDevice,
            VERTEX_BUFFER_SIZE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    renderer.vertexDataBuffer = VulkanCreateBuffer(renderer.device,
        renderer.physicalDevice, VERTEX_BUFFER_SIZE,
        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    renderer.indexUploadBuffer =
        VulkanCreateBuffer(renderer.device, renderer.physicalDevice,
            INDEX_BUFFER_SIZE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    renderer.indexBuffer = VulkanCreateBuffer(renderer.device,
        renderer.physicalDevice, INDEX_BUFFER_SIZE,
        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT |
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    renderer.cameraDataBuffer =
        VulkanCreateBuffer(renderer.device, renderer.physicalDevice,
            sizeof(CameraDataBuffer), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    renderer.modelMatrixBuffer =
        VulkanCreateBuffer(renderer.device, renderer.physicalDevice,
            MODEL_MATRIX_BUFFER_SIZE, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    renderer.textureUploadBuffer =
        VulkanCreateBuffer(renderer.device, renderer.physicalDevice,
            TEXTURE_UPLOAD_BUFFER_SIZE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    InitializeMemoryArena(&renderer.vertexDataUploadArena,
        renderer.vertexDataUploadBuffer.data, VERTEX_BUFFER_SIZE);
    InitializeMemoryArena(&renderer.indexDataUploadArena,
        renderer.indexUploadBuffer.data, INDEX_BUFFER_SIZE);

    // TODO: CLAMP_TO_EDGE sampler
    VkSamplerCreateInfo samplerCreateInfo = {
        VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO};
    samplerCreateInfo.magFilter = VK_FILTER_LINEAR;
    samplerCreateInfo.minFilter = VK_FILTER_LINEAR;
    samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerCreateInfo.anisotropyEnable = VK_TRUE;
    samplerCreateInfo.maxAnisotropy = 16.0f;
    samplerCreateInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerCreateInfo.unnormalizedCoordinates = VK_FALSE;
    samplerCreateInfo.compareEnable = VK_FALSE;
    samplerCreateInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerCreateInfo.mipLodBias = 0.0f;
    samplerCreateInfo.minLod = 0.0f;
    samplerCreateInfo.maxLod = 1000.0f;
    VK_CHECK(vkCreateSampler(
        renderer.device, &samplerCreateInfo, NULL, &renderer.defaultSampler));

    return renderer;
}

internal void UpdateModelMatricesBufferForScene(
    VulkanRenderer *renderer, RendererScene *scene)
{
    mat4 *modelMatrices = (mat4 *)renderer->modelMatrixBuffer.data;
    for (u32 i = 0; i < scene->count; i++)
    {
        RendererSceneElement *element = scene->elements + i;

        modelMatrices[i] = element->modelMatrix;
    }
}

internal void BuildCommandBufferForScene(
    VulkanRenderer *renderer, RendererScene *scene, u32 imageIndex)
{
    for (u32 i = 0; i < scene->count; i++)
    {
        RendererSceneElement *element = scene->elements + i;

        VulkanPipeline *pipeline = &renderer->meshPipeline;

        vkCmdBindPipeline(renderer->commandBuffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->handle);

        vkCmdBindDescriptorSets(renderer->commandBuffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->layout, 0, 1,
            &pipeline->descriptorSets[imageIndex], 0, NULL);

        vkCmdBindIndexBuffer(renderer->commandBuffer,
            renderer->indexBuffer.handle, element->indexDataOffset,
            VK_INDEX_TYPE_UINT32);

        MeshPushConstants pushConstants = {};
        pushConstants.modelMatrixIndex = i;
        pushConstants.vertexIndexOffset = element->vertexIndexOffset;

        vkCmdPushConstants(renderer->commandBuffer,
            pipeline->layout, VK_SHADER_STAGE_VERTEX_BIT, 0,
            sizeof(pushConstants), &pushConstants);

        vkCmdDrawIndexed(
            renderer->commandBuffer, element->indexCount, 1, 0, 0, 0);
    }

}

internal void RenderFrame(VulkanRenderer *renderer, RendererScene *scene)
{
    UpdateModelMatricesBufferForScene(renderer, scene);

    u32 imageIndex;
    VK_CHECK(vkAcquireNextImageKHR(renderer->device, renderer->swapchain.handle,
        0, renderer->acquireSemaphore, VK_NULL_HANDLE, &imageIndex));

    VK_CHECK(vkResetCommandPool(renderer->device, renderer->commandPool, 0));

    VkCommandBufferBeginInfo beginInfo = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    VK_CHECK(vkBeginCommandBuffer(renderer->commandBuffer, &beginInfo));

    // Execute render pass
    VkClearValue clearValues[2] = {};
    clearValues[0].color = {0.02f, 0.02f, 0.1f, 1.0f};
    clearValues[1].depthStencil = {1.0f, 0};

    VkRenderPassBeginInfo renderPassPresentBegin = {
        VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};
    renderPassPresentBegin.renderPass = renderer->renderPass;
    renderPassPresentBegin.framebuffer =
        renderer->swapchain.framebuffers[imageIndex];
    renderPassPresentBegin.renderArea.extent.width = renderer->swapchain.width;
    renderPassPresentBegin.renderArea.extent.height =
        renderer->swapchain.height;
    renderPassPresentBegin.clearValueCount = ArrayCount(clearValues);
    renderPassPresentBegin.pClearValues = clearValues;
    vkCmdBeginRenderPass(renderer->commandBuffer, &renderPassPresentBegin,
        VK_SUBPASS_CONTENTS_INLINE);

    // Set dynamic pipeline state
    VkViewport viewportPresent = {0, 0, (f32)renderer->swapchain.width,
        (f32)renderer->swapchain.height, 0.0f, 1.0f};
    VkRect2D scissorPresent = {
        0, 0, renderer->swapchain.width, renderer->swapchain.height};
    vkCmdSetViewport(renderer->commandBuffer, 0, 1, &viewportPresent);
    vkCmdSetScissor(renderer->commandBuffer, 0, 1, &scissorPresent);

    BuildCommandBufferForScene(renderer, scene, imageIndex);

    vkCmdEndRenderPass(renderer->commandBuffer);

    VK_CHECK(vkEndCommandBuffer(renderer->commandBuffer));

    VkPipelineStageFlags submitStageMask =
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submitInfo = {VK_STRUCTURE_TYPE_SUBMIT_INFO};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &renderer->acquireSemaphore;
    submitInfo.pWaitDstStageMask = &submitStageMask;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &renderer->commandBuffer;
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &renderer->releaseSemaphore;
    VK_CHECK(vkQueueSubmit(renderer->graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE));

    VkPresentInfoKHR presentInfo = {VK_STRUCTURE_TYPE_PRESENT_INFO_KHR};
    presentInfo.swapchainCount = 1;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &renderer->releaseSemaphore;
    presentInfo.pSwapchains = &renderer->swapchain.handle;
    presentInfo.pImageIndices = &imageIndex;
    VK_CHECK(vkQueuePresentKHR(renderer->presentQueue, &presentInfo));

    // FIXME: Remove this to allow CPU and GPU to run in parallel
    VK_CHECK(vkDeviceWaitIdle(renderer->device));
}

internal VertexPNT *AllocateVertexData(VulkanRenderer *renderer, u32 bytes)
{
    VertexPNT *vertices =
        (VertexPNT *)AllocateBytes(&renderer->vertexDataUploadArena, bytes);

    return vertices;
}

internal u32 *AllocateIndexData(VulkanRenderer *renderer, u32 bytes)
{
    u32 *indices = (u32 *)AllocateBytes(&renderer->indexDataUploadArena, bytes);
    return indices;
}
