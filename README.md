# 2023 Computer Graphics Hobby Project

## Build Instructions

Checkout submodules

```
git submodule init
git submodule update
```

Build Dependencies

```
build_dependencies.bat
```

Build App

```
mkdir build
cd build
cmake ..
cmake --build .
```

Run

```
bin/main.exe
```

## Goals

- [] Basic vulkan renderer
- [] Basic deferred lighting system
- [] Image based irradiance 
- [] Gamma correct HDR with really nice tone mapping
- [] Bloom
- [] Screen space ambient occlusion
- [] Sky rendering
- [] Soft shadows
