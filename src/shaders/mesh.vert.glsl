#version 450

struct VertexPNT
{
    float px, py, pz;
    float nx, ny, nz;
    float tx, ty;
};

layout(binding = 0) readonly buffer Vertices
{
    VertexPNT vertices[];
};

layout(binding = 1) uniform CameraDataBuffer {
    mat4 viewMatrices[8];
    mat4 projectionMatrices[8];
} cameraData;

layout(binding = 4) readonly buffer ModelMatrices
{
    mat4 modelMatrices[];
};

layout(push_constant) uniform PushConstants
{
    uint modelMatrixIndex;
    uint vertexIndexOffset;
    uint cameraIndex;
    int layer;
};

layout(location = 0) out vec2 fragTexCoord;

void main()
{
    vec3 inPosition = vec3(
            vertices[gl_VertexIndex + vertexIndexOffset].px,
            vertices[gl_VertexIndex + vertexIndexOffset].py,
            vertices[gl_VertexIndex + vertexIndexOffset].pz);

    vec3 inNormal = vec3(
            vertices[gl_VertexIndex + vertexIndexOffset].nx,
            vertices[gl_VertexIndex + vertexIndexOffset].ny,
            vertices[gl_VertexIndex + vertexIndexOffset].nz);

    vec2 inTextureCoord = vec2(
            vertices[gl_VertexIndex + vertexIndexOffset].tx,
            vertices[gl_VertexIndex + vertexIndexOffset].ty);

    mat4 modelMatrix = modelMatrices[modelMatrixIndex];

    gl_Position = cameraData.projectionMatrices[0] *
                  cameraData.viewMatrices[0] * modelMatrix *
                  vec4(inPosition, 1.0);

    fragTexCoord = inTextureCoord;
}
