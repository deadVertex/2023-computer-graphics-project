/* TODO
Basic Vulkan Renderer
- Textures!
    - Support multiple textures
    - Cube maps / skybox
- Render passes / HDR
- Mesh data management
- Shader management
- Texture management
*/
#include <cstdio>

// Config Options
#define ENABLE_VALIDATION_LAYERS
#define SHADER_PATH "shaders"

#define CAMERA_FOV 60.0f
#define CAMERA_NEAR_CLIP 0.01f


#ifdef PLATFORM_WINDOWS
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#elif defined(PLATFORM_LINUX)
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#endif

// Move to vulkan specific module?
#include <vulkan/vulkan.h>
#ifdef PLATFORM_WINDOWS
#include <vulkan/vulkan_win32.h>
#endif

#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#include "platform.h"
#include "input.h"
#include "math_utils.h"
#include "math_lib.h"
#include "mesh.h"
#include "image.h"

#include "vulkan_utils.h"
#include "vulkan_renderer.h"
#include "vulkan_renderer.cpp"

#include "mesh.cpp"

#include "asset_loader/asset_loader.h"

struct DebugReadFileResult
{
    void *contents;
    u32 length;
};

#define DebugReadEntireFile(NAME) DebugReadFileResult NAME(const char *path)
typedef DebugReadEntireFile(DebugReadEntireFileFunction);
#define DebugFreeFileMemory(NAME) void NAME(void *memory)
typedef DebugFreeFileMemory(DebugFreeFileMemoryFunction);

internal void* AllocateMemory(u64 size, u64 baseAddress = 0);
internal void FreeMemory(void *p);
internal DebugReadEntireFile(ReadEntireFile);

global GLFWwindow *g_Window;
global u32 g_FramebufferWidth = 1280;
global u32 g_FramebufferHeight = 720;

internal DebugLogMessage(LogMessage_)
{
    char buffer[256];
    va_list args;
    va_start(args, fmt);
    vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);
#ifdef PLATFORM_WINDOWS
    OutputDebugString(buffer);
    OutputDebugString("\n");
#endif
    puts(buffer);
}

internal void GlfwErrorCallback(int error, const char *description)
{
    LogMessage("GLFW Error (%d): %s.", error, description);
}

#ifdef PLATFORM_WINDOWS
internal void *AllocateMemory(u64 size, u64 baseAddress)
{
    void *result =
        VirtualAlloc((void*)baseAddress, size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    return result;
}

internal void FreeMemory(void *p)
{
    VirtualFree(p, 0, MEM_RELEASE);
}

DebugReadEntireFile(ReadEntireFile)
{
    DebugReadFileResult result = {};
    HANDLE file = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ, 0,
                              OPEN_EXISTING, 0, 0);
    if (file != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER tempSize;
        if (GetFileSizeEx(file, &tempSize))
        {
            result.length = SafeTruncateU64ToU32(tempSize.QuadPart);
            result.contents = AllocateMemory(result.length);

            if (result.contents)
            {
                DWORD bytesRead;
                if (!ReadFile(file, result.contents, result.length, &bytesRead,
                              0) ||
                    (result.length != bytesRead))
                {
                    LogMessage("Failed to read file %s", path);
                    FreeMemory(result.contents);
                    result.contents = NULL;
                    result.length = 0;
                }
            }
            else
            {
                LogMessage("Failed to allocate %d bytes for file %s",
                          result.length, path);
            }
        }
        else
        {
            LogMessage("Failed to read file length for file %s", path);
        }
        CloseHandle(file);
    }
    else
    {
        LogMessage("Failed to open file %s", path);
    }
    return result;
}

#elif defined(PLATFORM_LINUX)
internal void *AllocateMemory(u64 numBytes, u64 baseAddress)
{
    void *result = mmap((void *)baseAddress, numBytes, PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    Assert(result != MAP_FAILED);

    return result;
}

internal void FreeMemory(void *p)
{
    munmap(p, 0);
}

// NOTE: bytesToRead must equal the size of the file, if great we will enter an
// infinite loop.
internal bool ReadFile(int file, void *buf, int bytesToRead)
{
    while (bytesToRead)
    {
        int bytesRead = read(file, buf, bytesToRead);
        if (bytesRead == -1)
        {
            return false;
        }
        bytesToRead -= bytesRead;
        buf = (u8 *)buf + bytesRead;
    }
    return true;
}

internal DebugReadEntireFile(ReadEntireFile)
{
    DebugReadFileResult result = {};
    int file = open(path, O_RDONLY);
    if (file != -1)
    {
        struct stat fileStatus;
        if (fstat(file, &fileStatus) != -1)
        {
            result.length = SafeTruncateU64ToU32(fileStatus.st_size);
            result.contents = AllocateMemory(result.length);
            if (result.contents)
            {
                if (!ReadFile(file, result.contents, result.length))
                {
                    LogMessage("Failed to read file %s", path);
                    FreeMemory(result.contents);
                    result.contents = nullptr;
                    result.length = 0;
                }
            }
            else
            {
                LogMessage("Failed to allocate %d bytes for file %s",
                          result.length, path);
                result.length = 0;
            }
        }
        else
        {
            LogMessage("Failed to read file size for file %s", path);
        }
        close(file);
    }
    else
    {
        LogMessage("Failed to open file %s", path);
    }
    return result;
}
#endif

#define KEY_HELPER(NAME)                                                       \
    case GLFW_KEY_##NAME:                                                      \
        return KEY_##NAME;
internal i32 ConvertKey(int key)
{
    if (key >= GLFW_KEY_SPACE && key <= GLFW_KEY_GRAVE_ACCENT)
    {
        return key;
    }
    switch (key)
    {
        KEY_HELPER(BACKSPACE);
        KEY_HELPER(TAB);
        KEY_HELPER(INSERT);
        KEY_HELPER(HOME);
        KEY_HELPER(PAGE_UP);
    // Can't use KEY_HELPER( DELETE ) as windows has a #define for DELETE
    case GLFW_KEY_DELETE:
        return KEY_DELETE;
        KEY_HELPER(END);
        KEY_HELPER(PAGE_DOWN);
        KEY_HELPER(ENTER);

        KEY_HELPER(LEFT_SHIFT);
    case GLFW_KEY_LEFT_CONTROL:
        return KEY_LEFT_CTRL;
        KEY_HELPER(LEFT_ALT);
        KEY_HELPER(RIGHT_SHIFT);
    case GLFW_KEY_RIGHT_CONTROL:
        return KEY_RIGHT_CTRL;
        KEY_HELPER(RIGHT_ALT);

        KEY_HELPER(LEFT);
        KEY_HELPER(RIGHT);
        KEY_HELPER(UP);
        KEY_HELPER(DOWN);

        KEY_HELPER(ESCAPE);

        KEY_HELPER(F1);
        KEY_HELPER(F2);
        KEY_HELPER(F3);
        KEY_HELPER(F4);
        KEY_HELPER(F5);
        KEY_HELPER(F6);
        KEY_HELPER(F7);
        KEY_HELPER(F8);
        KEY_HELPER(F9);
        KEY_HELPER(F10);
        KEY_HELPER(F11);
        KEY_HELPER(F12);
    case GLFW_KEY_KP_0:
        return KEY_NUM0;
    case GLFW_KEY_KP_1:
        return KEY_NUM1;
    case GLFW_KEY_KP_2:
        return KEY_NUM2;
    case GLFW_KEY_KP_3:
        return KEY_NUM3;
    case GLFW_KEY_KP_4:
        return KEY_NUM4;
    case GLFW_KEY_KP_5:
        return KEY_NUM5;
    case GLFW_KEY_KP_6:
        return KEY_NUM6;
    case GLFW_KEY_KP_7:
        return KEY_NUM7;
    case GLFW_KEY_KP_8:
        return KEY_NUM8;
    case GLFW_KEY_KP_9:
        return KEY_NUM9;
    case GLFW_KEY_KP_DECIMAL:
        return KEY_NUM_DECIMAL;
    case GLFW_KEY_KP_DIVIDE:
        return KEY_NUM_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY:
        return KEY_NUM_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT:
        return KEY_NUM_MINUS;
    case GLFW_KEY_KP_ADD:
        return KEY_NUM_PLUS;
    case GLFW_KEY_KP_ENTER:
        return KEY_NUM_ENTER;
    }
    return KEY_UNKNOWN;
}


void KeyCallback(GLFWwindow *window, int glfwKey, int scancode, int action, int mods)
{
    GameInput *input =
            (GameInput *)glfwGetWindowUserPointer(window);

    i32 key = ConvertKey(glfwKey);
    if (key != KEY_UNKNOWN)
    {
        Assert(key < MAX_KEYS);
        if (action == GLFW_PRESS)
        {
            input->buttonStates[key].isDown = true;
        }
        else if (action == GLFW_RELEASE)
        {
            input->buttonStates[key].isDown = false;
        }
        // else: ignore key repeat messages
    }
}

internal void MouseButtonCallback(
    GLFWwindow *window, int button, int action, int mods)
{
    GameInput *input =
            (GameInput *)glfwGetWindowUserPointer(window);

    if (button == GLFW_MOUSE_BUTTON_LEFT)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown = (action == GLFW_PRESS);
    }
    if (button == GLFW_MOUSE_BUTTON_MIDDLE)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_MIDDLE].isDown = (action == GLFW_PRESS);
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown = (action == GLFW_PRESS);
    }
}

global f64 g_PrevMousePosX;
global f64 g_PrevMousePosY;

internal void CursorPositionCallback(GLFWwindow *window, f64 xPos, f64 yPos)
{
    GameInput *input = (GameInput *)glfwGetWindowUserPointer(window);

    // Unfortunately GLFW doesn't seem to provide a way to get mouse motion
    // rather than cursor position so we have to compute the relative motion
    // ourselves.
    f64 relX = xPos - g_PrevMousePosX;
    f64 relY = yPos - g_PrevMousePosY;
    g_PrevMousePosX = xPos;
    g_PrevMousePosY = yPos;

    input->mouseRelPosX = (i32)Floor((f32)relX);
    input->mouseRelPosY = (i32)Floor((f32)relY);
    input->mousePosX = (i32)Floor((f32)xPos);
    input->mousePosY = (i32)Floor((f32)yPos);
}

internal VkShaderModule LoadShader(VulkanRenderer *renderer, const char *path)
{
    char fullPath[256];
    snprintf(fullPath, sizeof(fullPath), "%s/%s", SHADER_PATH, path);

    DebugReadFileResult fileData = ReadEntireFile(fullPath);
    Assert(fileData.contents);
    Assert(fileData.length % 4 == 0);

    VkShaderModuleCreateInfo createInfo = {VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO};
    createInfo.codeSize = fileData.length;
    createInfo.pCode = (u32 *)fileData.contents;

    VkShaderModule shaderModule;
    VK_CHECK(vkCreateShaderModule(
        renderer->device, &createInfo, NULL, &shaderModule));
    return shaderModule;
}

internal HdrImage LoadImageFromDisk(
    const char *relativePath, MemoryArena *imageDataArena, const char *assetDir)
{
    HdrImage result = {};

    char fullPath[256];
    snprintf(fullPath, sizeof(fullPath), "%s/%s", assetDir, relativePath);

    HdrImageData tempImage = {};
    if(LoadHdrImage(&tempImage, fullPath) == 0)
    {
        result =
            AllocateImage(tempImage.width, tempImage.height, imageDataArena);
        CopyMemory(result.pixels, tempImage.pixels,
            result.width * result.height * sizeof(vec4));
        FreeHdrImage(tempImage);
    }
    else
    {
        LogMessage("Failed to load image %s", fullPath);
    }

    return result;
}

// FIXME: Needs more thought
struct Mesh
{
    u32 vertexIndexOffset;
    u32 indexDataOffset;
    u32 indexCount;
};

struct AssetSystem
{
    Mesh meshes[64];
};

internal void LoadAssets(
    VulkanRenderer *renderer, AssetSystem *assetSystem, MemoryArena *assetArena)
{
    {
        // Populate vertex data buffer
        VertexPNT *vertices = AllocateVertexData(renderer, sizeof(VertexPNT) * 3);
        vertices[0].position = Vec3(-0.5, -0.5, 0.0);
        vertices[0].normal = Vec3(0.0, 0.0, 1.0);
        vertices[0].textureCoord = Vec2(0.0, 0.0);
        vertices[1].position = Vec3(0.5, -0.5, 0.0);
        vertices[1].normal = Vec3(0.0, 0.0, 1.0);
        vertices[1].textureCoord = Vec2(1.0, 0.0);
        vertices[2].position = Vec3(0.0, 0.5, 0.0);
        vertices[2].normal = Vec3(0.0, 0.0, 1.0);
        vertices[2].textureCoord = Vec2(0.5, 1.0);

        // Populate index buffer
        u32 *indices = AllocateIndexData(renderer, sizeof(u32) * 3);
        indices[0] = 0;
        indices[1] = 1;
        indices[2] = 2;

        Mesh mesh = {};
        mesh.vertexIndexOffset = 0;
        mesh.indexDataOffset = 0;
        mesh.indexCount = 3;

        assetSystem->meshes[0] = mesh;
    }

    const char *assetDir = "../assets";
    MeshData mesh = LoadMesh("bunny.obj", assetArena, assetDir);
    {
        VertexPNT *vertices =
            AllocateVertexData(renderer, sizeof(VertexPNT) * mesh.vertexCount);
        memcpy(vertices, mesh.vertices, sizeof(VertexPNT) * mesh.vertexCount);

        u32 *indices = AllocateIndexData(renderer, sizeof(u32) * mesh.indexCount);
        memcpy(indices, mesh.indices, sizeof(u32) * mesh.indexCount);

        Mesh gpuMesh = {};
        gpuMesh.vertexIndexOffset = SafeTruncateU64ToU32(
            vertices - (VertexPNT*)renderer->vertexDataUploadArena.base);
        gpuMesh.indexDataOffset = SafeTruncateU64ToU32(
            (u8*)indices - (u8*)renderer->indexDataUploadArena.base);
        gpuMesh.indexCount = mesh.indexCount;

        assetSystem->meshes[1] = gpuMesh;
    }

    // Upload Vertex data to GPU
    VulkanCopyBuffer(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->vertexDataUploadBuffer.handle,
        renderer->vertexDataBuffer.handle,
        renderer->vertexDataUploadArena.size);

    VulkanCopyBuffer(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->indexUploadBuffer.handle,
        renderer->indexBuffer.handle, renderer->indexDataUploadArena.size);

    HdrImage image = LoadImageFromDisk(
        "Ground067_2K-JPG_Color.jpg", assetArena, assetDir);
    renderer->testImage = UploadHdrImageToGPU(renderer, image);

    // FIXME: This writes the descriptor sets so we need to call it after the image is created.
    renderer->meshPipeline =
        CreatePipeline(renderer, LoadShader(renderer, "mesh.vert.spv"),
            LoadShader(renderer, "mesh.frag.spv"));

}

inline void AddToScene(RendererScene *scene, mat4 modelMatrix, Mesh mesh)
{
    Assert(scene->count < ArrayCount(scene->elements));
    RendererSceneElement *elem = scene->elements + scene->count++;

    elem->modelMatrix = modelMatrix;
    elem->vertexIndexOffset = mesh.vertexIndexOffset;
    elem->indexDataOffset = mesh.indexDataOffset;
    elem->indexCount = mesh.indexCount;
}

internal void LoadScene(RendererScene *scene, AssetSystem *assetSystem)
{
    AddToScene(scene, Identity(), assetSystem->meshes[0]);
    AddToScene(scene, Translate(Vec3(1, 0, 0)), assetSystem->meshes[0]);
    AddToScene(scene, Translate(Vec3(-1, 0, 0)), assetSystem->meshes[1]);
}

internal void ShowMouseCursor(b32 isVisible)
{
    glfwSetInputMode(g_Window, GLFW_CURSOR,
        isVisible ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
}

struct FreeRoamCamera
{
    vec3 position;
    vec3 velocity;
    vec3 rotation;
};

internal void UpdateFreeRoamCamera(
    FreeRoamCamera *camera, GameInput *input, f32 dt)
{
    vec3 position = camera->position;
    vec3 velocity = camera->velocity;
    vec3 rotation = camera->rotation;

    f32 speed = 20.0f; //200.0f;
    f32 friction = 18.0f;

    f32 sens = 0.005f;
    f32 mouseX = -input->mouseRelPosY * sens;
    f32 mouseY = -input->mouseRelPosX * sens;

    vec3 newRotation = Vec3(mouseX, mouseY, 0.0f);

    if (input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown)
    {
        rotation += newRotation;
        ShowMouseCursor(false);
    }
    else
    {
        ShowMouseCursor(true);
    }

    rotation.x = Clamp(rotation.x, -PI * 0.5f, PI * 0.5f);

    if (rotation.y > 2.0f * PI)
    {
        rotation.y -= 2.0f * PI;
    }
    if (rotation.y < 2.0f * -PI)
    {
        rotation.y += 2.0f * PI;
    }

    f32 forwardMove = 0.0f;
    f32 rightMove = 0.0f;
    if (input->buttonStates[KEY_W].isDown)
    {
        forwardMove = -1.0f;
    }

    if (input->buttonStates[KEY_S].isDown)
    {
        forwardMove = 1.0f;
    }

    if (input->buttonStates[KEY_A].isDown)
    {
        rightMove = -1.0f;
    }
    if (input->buttonStates[KEY_D].isDown)
    {
        rightMove = 1.0f;
    }

    if (input->buttonStates[KEY_LEFT_SHIFT].isDown)
    {
        speed *= 10.0f;
    }

    mat4 rotationMatrix = RotateY(rotation.y) * RotateX(rotation.x);

    vec3 forward = (rotationMatrix * Vec4(0, 0, 1, 0)).xyz;
    vec3 right = (rotationMatrix * Vec4(1, 0, 0, 0)).xyz;

    vec3 targetDir = Normalize(forward * forwardMove + right * rightMove);

    velocity -= velocity * friction * dt;
    velocity += targetDir * speed * dt;

    position = position + velocity * dt;

    camera->position = position;
    camera->velocity = velocity;
    camera->rotation = rotation;
}

internal void UpdateCameraDataBuffer(
    VulkanRenderer *renderer, FreeRoamCamera camera)
{
    CameraDataBuffer *buffer =
        (CameraDataBuffer *)renderer->cameraDataBuffer.data;

    buffer->viewMatrices[0] = RotateX(-camera.rotation.x) *
                              RotateY(-camera.rotation.y) *
                              Translate(-camera.position);

    f32 aspect =
        (f32)renderer->swapchain.width / (f32)renderer->swapchain.height;

    // Vulkan specific correction matrix
    mat4 correctionMatrix = {};
    correctionMatrix.columns[0] = Vec4(1, 0, 0, 0);
    correctionMatrix.columns[1] = Vec4(0, -1, 0, 0);
    correctionMatrix.columns[2] = Vec4(0, 0, 0.5f, 0);
    correctionMatrix.columns[3] = Vec4(0, 0, 0.5f, 1);
    buffer->projectionMatrices[0] =
        correctionMatrix *
        Perspective(CAMERA_FOV, aspect, CAMERA_NEAR_CLIP, 100.0f);
}

int main(int argc, char **argv)
{
    LogMessage = &LogMessage_;

    LogMessage("2023 Computer Graphics Hobby Project");

    LogMessage("Compiled agist GLFW %i.%i.%i", GLFW_VERSION_MAJOR,
           GLFW_VERSION_MINOR, GLFW_VERSION_REVISION);

    i32 major, minor, revision;
    glfwGetVersion(&major, &minor, &revision);
    LogMessage("Running against GLFW %i.%i.%i", major, minor, revision);
    LogMessage("%s", glfwGetVersionString());

    glfwSetErrorCallback(GlfwErrorCallback);
    if (!glfwInit())
    {
        LogMessage("Failed to initialize GLFW!");
        return -1;
    }

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    g_Window = glfwCreateWindow(g_FramebufferWidth,
        g_FramebufferHeight, "2023 Computer Graphics Hobby Project", NULL, NULL);
    Assert(g_Window != NULL);
    GameInput input = {};
    glfwSetWindowUserPointer(g_Window, &input);
    glfwSetKeyCallback(g_Window, KeyCallback);
    glfwSetMouseButtonCallback(g_Window, MouseButtonCallback);
    glfwSetCursorPosCallback(g_Window, CursorPositionCallback);

    // Create application memory arena
    u32 assetArenaSize = Megabytes(256);
    u32 applicationMemorySize = assetArenaSize;
    MemoryArena applicationMemoryArena = {};
    InitializeMemoryArena(&applicationMemoryArena,
        AllocateMemory(applicationMemorySize), applicationMemorySize);

    MemoryArena assetArena =
        SubAllocateArena(&applicationMemoryArena, assetArenaSize);

    VulkanRenderer renderer = CreateVulkanRenderer(g_Window);
    
    AssetSystem assetSystem = {};
    LoadAssets(&renderer, &assetSystem, &assetArena);

    RendererScene scene = {};
    LoadScene(&scene, &assetSystem);

    FreeRoamCamera camera = {};

    f32 prevFrameTime = 0.0f;
    while (!glfwWindowShouldClose(g_Window))
    {
        f32 dt = prevFrameTime;
        dt = Min(dt, 0.25f);

        f64 frameStart = glfwGetTime();
        InputBeginFrame(&input);
        glfwPollEvents();

        UpdateFreeRoamCamera(&camera, &input, dt);
        UpdateCameraDataBuffer(&renderer, camera);

        RenderFrame(&renderer, &scene);

        prevFrameTime = (f32)(glfwGetTime() - frameStart);
    }

    return 0;
}
