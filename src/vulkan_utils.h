#pragma once

struct VulkanBuffer
{
    VkBuffer handle;
    VkDeviceMemory memory;
    void *data;
};

struct VulkanImage
{
    VkImage handle;
    VkDeviceMemory memory;
    VkImageView view;
};

enum
{
    Primitive_Triangle,
    Primitive_Line,
};

enum
{
    PolygonMode_Fill,
    PolygonMode_Line,
};

enum
{
    CullMode_None,
    CullMode_Front,
    CullMode_Back,
};

struct VulkanPipelineDefinition
{
    u32 vertexStride;
    u32 primitive;
    u32 polygonMode;
    u32 cullMode;
    b32 depthTestEnabled;
    b32 depthWriteEnabled;
    b32 alphaBlendingEnabled;
};

struct VulkanHdrSwapchainFramebuffer
{
    VulkanImage depth;
    VulkanImage color;
    VkFramebuffer handle;
};

struct VulkanHdrSwapchain
{
    VulkanHdrSwapchainFramebuffer framebuffers[2];
    u32 imageCount;
};

struct VulkanQueueFamilyIndices
{
    u32 graphicsQueue;
    u32 presentQueue;
};
