#pragma once

#define VERTEX_BUFFER_SIZE Megabytes(512)
#define INDEX_BUFFER_SIZE Megabytes(2)
#define TEXTURE_UPLOAD_BUFFER_SIZE Megabytes(64)
#define MODEL_MATRIX_BUFFER_SIZE Megabytes(2)

struct CameraDataBuffer
{
    mat4 viewMatrices[8];
    mat4 projectionMatrices[8];
};

struct VulkanSwapchain
{
    VkSwapchainKHR handle;
    VkImageView imageViews[2]; // TODO: Store as array of VulkanImages
    VkImage images[2];
    VkFramebuffer framebuffers[2];
    u32 imageCount;
    u32 width;
    u32 height;

    VulkanImage depthImage;
};

struct VulkanPipeline
{
    VkShaderModule vertexShader;
    VkShaderModule FragmentShader;

    VkPipelineLayout layout;
    VkPipeline handle;
    VkDescriptorSetLayout descriptorSetLayout;
    VkDescriptorSet descriptorSets[2];
};

struct VulkanMesh
{
    u32 vertexIndexOffset;
    u32 vertexDataLength;

    u32 indexDataOffset;
    u32 indexDataLength;
};

struct VulkanRenderer
{
    VkInstance instance;
    VkDebugUtilsMessengerEXT messenger;
    VkPhysicalDevice physicalDevice;
    VkSurfaceKHR surface;

    VulkanQueueFamilyIndices queueFamilyIndices;
    VkDevice device;

    VkQueue graphicsQueue;
    VkQueue presentQueue;

    VkRenderPass renderPass;
    VulkanSwapchain swapchain;

    VkSemaphore acquireSemaphore;
    VkSemaphore releaseSemaphore;

    VkCommandPool commandPool;
    VkDescriptorPool descriptorPool;

    VkCommandBuffer commandBuffer;

    VulkanBuffer vertexDataUploadBuffer;
    VulkanBuffer vertexDataBuffer;

    VulkanBuffer indexUploadBuffer;
    VulkanBuffer indexBuffer;

    MemoryArena vertexDataUploadArena;
    MemoryArena indexDataUploadArena;

    VulkanBuffer cameraDataBuffer;
    VulkanBuffer modelMatrixBuffer;

    VulkanBuffer textureUploadBuffer;

    VkPipelineCache pipelineCache;

    VkSampler defaultSampler;

    VulkanPipeline meshPipeline;

    VulkanImage testImage;
};

struct MeshPushConstants
{
    u32 modelMatrixIndex;
    u32 vertexIndexOffset;
    u32 cameraIndex;
    i32 layer;
};

struct RendererSceneElement
{
    u32 pipelineId;
    u32 drawCallId;

    u32 vertexIndexOffset;
    u32 indexDataOffset;
    u32 indexCount;

    mat4 modelMatrix;
};

struct RendererScene
{
    RendererSceneElement elements[64];
    u32 count;
};
