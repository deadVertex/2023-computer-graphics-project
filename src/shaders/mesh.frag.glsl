#version 450

layout(location = 0) out vec4 outputColor;

layout(binding = 2) uniform sampler defaultSampler;
layout(binding = 3) uniform texture2D albedoTexture;

layout(location = 0) in vec2 fragTexCoord;

void main()
{
    vec3 baseColor =
        texture(sampler2D(albedoTexture, defaultSampler), fragTexCoord).rgb;
    outputColor = vec4(baseColor, 1);
}
